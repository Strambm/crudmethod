<?php

namespace Cetria\Enums;

class CrudMethod 
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const PATCH = 'PATCH';
    const DELETE = 'DELETE';

    public static function methods(): array
    {
        return [
            self::GET,
            self::POST,
            self::PUT,
            self::PATCH,
            self::DELETE,
        ];
    }

    public static function exist(string $method): bool
    {
        return in_array(strtoupper($method), self::methods());
    }
}
